#include <FunctionsLAB6.hpp>

namespace ul
{
	void Read(int& n, int& m, int mas[N][M])
	{
		std::cin >> n >> m;
		for (int i = 0; i < n; i++)
		{
			for (int j = 0; j < m; j++)
			{
				std::cin >> mas[i][j];
			}
		}
	}
	//����������
	bool SameColumns(int n, int m, int mas[N][M])
	{
		for (int k = 0; k < m - 1; k++)
		{ 			
			for (int l = k + 1; l < m; l++)
			{		
				bool same = true;
				for (int i = 0; i<n; i++)
				{ 					
					if (mas[i][k] != mas[i][l])
					{
						same = false;
						break;
					}
				}
				if (same)
				{
					return true;
				}
			}
		}
		return false;
	}

	bool IsAbsPrime(int x)
	{
		for (int d = 2; d <= sqrt(abs(x)); d++)
		{
			if (abs(x) % d == 0)
				return false;
		}
		return true;
	}

	bool ConsistsAbsPrime(int n, int m, int mas[N][M])
	{
		for (int i = 0; i < n; i++)
		{
			for (int j = 0; j < m; j++)
			{
				if (ul::IsAbsPrime(mas[i][j]))
					return true;
			}
		}
		return false;
	}

	int SumAbsRow(int i, int m, int mas[N][M])//i - ����� ������, m - ���-�� ��-��� � ������
	{
		int summ = 0;
		for (int j = 0; j < m; j++)
		{
			summ += abs(mas[i][j]);
		}
		return summ;
	}

	void SortBySum(int n, int m, int mas[N][M])
	{
		for (int i = 0; i < n - 1; i++)
		{
			for (int j = i + 1; j < n; j++)
			{
				if (ul::SumAbsRow(i, m, mas) > ul::SumAbsRow(j, m, mas))
					std::swap(mas[i], mas[j]);
			}
		}
	}

	void Write(int n, int m, int mas[N][M])
	{
		std::cout << n << " " << m << std::endl;
		for (int i = 0; i < n; i++)
		{
			for (int j = 0; j < m; j++)
			{
				std::cout << mas[i][j] << " ";
			}
			std::cout << '\n';
		}
	}
}