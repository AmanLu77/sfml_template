#pragma once
#include <iostream>
const int N = 100;
const int M = 100;

namespace ul
{
	void Read(int& n, int& m, int mas[N][M]);

	bool SameColumns(int n, int m, int mas[N][M]);

	bool IsAbsPrime(int x);

	bool ConsistsAbsPrime(int n, int m, int mas[N][M]);

	int SumAbsRow(int i, int m, int mas[N][M]);

	void SortBySum(int n, int m, int mas[N][M]);

	void Write(int n, int m, int mas[N][M]);
}